import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import(`./layouts/user/user.module`).then(m => m.UserModule)
    },
    {
        path: 'dashboard',
        loadChildren: () => import(`./layouts/admin/admin.module`).then(m => m.AdminModule)
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
