import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  get(url) {
    return this.http.get(url);
  }
  post(url, data, headers) {
    return this.http.post(url, data, headers);
  }
  put(url, data, headers) {
    return this.http.put(url, data, headers);
  }
  delete(url, headers) {
    return this.http.delete(url, headers);
  }
}
