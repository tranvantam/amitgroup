import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user.component';

const routes: Routes = [
    {
        path: '',
        component: UserComponent,
        children: [
            {
                path: '',
                loadChildren: () => import(`../../modules/home/home.module`).then(m => m.HomeModule)
            },
            {
                path: 'about',
                loadChildren: () => import(`../../modules/about/about.module`).then(m => m.AboutModule)
            },
            {
                path: 'careers',
                loadChildren: () => import(`../../modules/careers/careers.module`).then(m => m.CareersModule)
            },
            {
                path: 'contact',
                loadChildren: () => import(`../../modules/contact/contact.module`).then(m => m.ContactModule)
            },
            {
                path: 'product',
                loadChildren: () => import(`../../modules/product/product.module`).then(m => m.ProductModule)
            },
            {
                path: 'service',
                loadChildren: () => import(`../../modules/service/service.module`).then(m => m.ServiceModule)
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule { }
