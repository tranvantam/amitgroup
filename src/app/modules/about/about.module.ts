import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './about.component';
import { AboutBannerComponent } from './components/about-banner/about-banner.component';
import { SomethingComponent } from './components/something/something.component';
import { ListOurComponent } from './components/list-our/list-our.component';
import { OurLeadershipComponent } from './components/our-leadership/our-leadership.component';
// === import pipe === //
import { AboutPipe } from './pipes/about.pipe';
// === import service === //
import { AboutService } from './services/about.service';
// === import directive === //
import { AboutDirective } from './directives/about.directive';
/* ---------------------------------------------------------------------------------
*
*   pipe/service/directive thực hiện các chức năng riêng chỉ áp dụng cho about
*   Khai báo trong @NgModule phạm vi sẽ từ about module hoặc con của about module
*
* -------------------------------------------------------------------------------- */


@NgModule({
    declarations: [
        AboutComponent,
        AboutBannerComponent,
        SomethingComponent,
        ListOurComponent,
        OurLeadershipComponent,
        AboutPipe,
        AboutDirective
    ],
    imports: [
        CommonModule,
        SharedModule,
        AboutRoutingModule
    ],
    providers: [AboutService]
})
export class AboutModule { }
