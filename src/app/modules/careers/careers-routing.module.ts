import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CareersApplyComponent } from './careers-apply/careers-apply.component';
import { CareersHomeComponent } from './careers-home/careers-home.component';
import { CareersComponent } from './careers.component';

const routes: Routes = [
  {
    path: '',
    component: CareersComponent,
    children: [
      {
        path: '',
        component: CareersHomeComponent
      },
      {
        path: 'apply',
        component: CareersApplyComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CareersRoutingModule { }
