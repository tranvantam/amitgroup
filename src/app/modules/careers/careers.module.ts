import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { CareersRoutingModule } from './careers-routing.module';
import { CareersComponent } from './careers.component';
import { CareersHomeComponent } from './careers-home/careers-home.component';
import { CareersApplyComponent } from './careers-apply/careers-apply.component';
import { BannerComponent } from './careers-home/components/banner/banner.component';
import { SixReasonsComponent } from './careers-home/components/six-reasons/six-reasons.component';
import { AvailableComponent } from './careers-home/components/available/available.component';
import { ListJobsComponent } from './careers-home/components/list-jobs/list-jobs.component';
import { FormIntershipComponent } from './careers-home/components/form-intership/form-intership.component';
import { CareersDirective } from './directives/careers.directive';
import { CareersPipe } from './pipes/careers.pipe';
import { CareersService } from './services/careers.service';
/* ---------------------------------------------------------------------------------
*
*   pipe/service/directive thực hiện các chức năng riêng chỉ áp dụng cho careers
*   Khai báo trong @NgModule phạm vi sẽ từ careers module hoặc con của careers module
*
* -------------------------------------------------------------------------------- */

@NgModule({
  declarations: [CareersComponent, CareersHomeComponent, CareersApplyComponent, BannerComponent, SixReasonsComponent, AvailableComponent, ListJobsComponent, FormIntershipComponent, CareersDirective, CareersPipe],
  imports: [
    CommonModule,
    SharedModule,
    CareersRoutingModule
  ],
  providers: [CareersService]
})
export class CareersModule { }
