import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact.component';
import { FormComponent } from './components/form/form.component';
import { WeHereComponent } from './components/we-here/we-here.component';
import { ContactPipe } from './pipes/contact.pipe';
import { ContactDirective } from './directives/contact.directive';
import { ContactService } from './services/contact.service';
/* ---------------------------------------------------------------------------------
*
*   pipe/service/directive thực hiện các chức năng riêng chỉ áp dụng cho contact
*   Khai báo trong @NgModule phạm vi sẽ từ contact module hoặc con của contact module
*
* -------------------------------------------------------------------------------- */

@NgModule({
  declarations: [ContactComponent, FormComponent, WeHereComponent, ContactPipe, ContactDirective],
  imports: [
    CommonModule,
    SharedModule,
    ContactRoutingModule
  ],
  providers: [ContactService]
})
export class ContactModule { }
