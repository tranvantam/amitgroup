import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HomeBannerComponent } from './components/home-banner/home-banner.component';
import { DesignDevComponent } from './components/design-dev/design-dev.component';
import { ModernTechnologyComponent } from './components/modern-technology/modern-technology.component';
import { SevenStepComponent } from './components/seven-step/seven-step.component';
import { AweProComponent } from './components/awe-pro/awe-pro.component';
import { HomeDirective } from './directives/home.directive';
import { HomePipe } from './pipes/home.pipe';
import { HomeService } from './services/home.service';
/* ---------------------------------------------------------------------------------
*
*   pipe/service/directive thực hiện các chức năng riêng chỉ áp dụng cho home
*   Khai báo trong @NgModule phạm vi sẽ từ home module hoặc con của home module
*
* -------------------------------------------------------------------------------- */

@NgModule({
  declarations: [
    HomeComponent, 
    HomeBannerComponent, 
    DesignDevComponent, 
    ModernTechnologyComponent, 
    SevenStepComponent, 
    AweProComponent, HomeDirective, HomePipe
  ],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule
  ],
  providers: [HomeService]
})
export class HomeModule { }
