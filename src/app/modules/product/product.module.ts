import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductPipe } from './pipes/product.pipe';
import { ProductDirective } from './directives/product.directive';
import { ProductService } from './services/product.service';
/* ---------------------------------------------------------------------------------
*
*   pipe/service/directive thực hiện các chức năng riêng chỉ áp dụng cho product
*   Khai báo trong @NgModule phạm vi sẽ từ product module hoặc con của product module
*
* -------------------------------------------------------------------------------- */


@NgModule({
  declarations: [ProductComponent, ProductListComponent, ProductDetailsComponent, ProductPipe, ProductDirective],
  imports: [
    CommonModule,
    SharedModule,
    ProductRoutingModule
  ],
  providers: [ProductService]
})
export class ProductModule { }
