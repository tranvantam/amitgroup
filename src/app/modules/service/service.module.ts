import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ServiceRoutingModule } from './service-routing.module';
import { ServiceComponent } from './service.component';
import { OurServicesComponent } from './components/our-services/our-services.component';
import { DifferenceComponent } from './components/difference/difference.component';
import { ServicePipe } from './pipes/service.pipe';
import { ServiceDirective } from './directives/service.directive';
import { ServiceService } from './services/service.service';
/* ---------------------------------------------------------------------------------
*
*   pipe/service/directive thực hiện các chức năng riêng chỉ áp dụng cho servie
*   Khai báo trong @NgModule phạm vi sẽ từ servie module hoặc con của servie module
*
* -------------------------------------------------------------------------------- */


@NgModule({
  declarations: [ServiceComponent, OurServicesComponent, DifferenceComponent, ServicePipe, ServiceDirective],
  imports: [
    CommonModule,
    SharedModule,
    ServiceRoutingModule
  ],
  providers: [ServiceService]
})
export class ServiceModule { }
