import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { PartnerComponent } from './components/partner/partner.component';
import { HelpYouComponent } from './components/help-you/help-you.component';
import { CountComponent } from './components/count/count.component';
import { TestDirective } from './directives/test.directive';
import { TestPipe } from './pipes/test.pipe';


@NgModule({
  declarations: [PartnerComponent, HelpYouComponent, CountComponent, TestDirective, TestPipe],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [    
    PartnerComponent,
    HelpYouComponent,
    CountComponent,
    MaterialModule
  ]
})
export class SharedModule { }
